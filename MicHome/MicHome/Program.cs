﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicHome
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Student();
            Person p1 = new Teacher();
            Grup g1 = new Grup();


            g1.TeachGrup = new Teacher() { Age = 35, Name = "Artyom", SureName = "Tonoyan" };
            g1.Name = "Mic3009";
            g1.StudensGrup = new List<Student>();
            g1.StudensGrup.Add(new Student() { Name = "Tigran", SureName = "Babayan", Age = 27 });
            g1.StudensGrup.Add(new Student() { Name = "A1", SureName = "A1yan", Age = 21 });
            g1.StudensGrup.Add(new Student() { Name = "A2", SureName = "A2yan", Age = 25 });
            g1.StudensGrup.Add(new Student() { Name = "A3", SureName = "A3yan", Age = 26 });

            g1.PrintPersonsOfGrup(g1);
            Console.ReadKey();



        }

    }
    class Grup
    {
        public List<Student> StudensGrup { get; set; }
        public string Name { get; set; }
        public Teacher TeachGrup { get; set; }
        public void PrintPersonsOfGrup(Grup grup)
        {
            Console.WriteLine($"****Teacher:{TeachGrup.FullName},Name of Grup:{grup.Name}****");
            Console.WriteLine("Study in grup");
            Console.WriteLine("-------------------------------------------");
            foreach (var item in grup.StudensGrup)
            {
                Console.WriteLine(item.FullName);
            }
        }
    }
    abstract class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string SureName { get; set; }
        public string FullName { get => $"{Name} {SureName}"; }
        public virtual void print()
        {
            Console.WriteLine($"{FullName} {Age}");
        }
    }
    class Student : Person
    {
        public int Course { get; set; }
        public override void print()
        {
            Console.WriteLine($"{FullName} {Age} ,Course:{Course}");
        }
    }
    class Teacher : Person

    {
        public int Experience { get; set; }//ashxatanqain porc
        public override void print()
        {
            Console.WriteLine($"{FullName} {Age} ,Experience:{Experience}");
        }
    }
}
